import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LibraryService } from '../services/library.service';

@Component({
  selector: 'app-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.scss']
})
export class GenresComponent implements OnInit {
  title: string;
  movies: Object;

  constructor(
    private _LibraryServices: LibraryService,
    private router: ActivatedRoute ) {
  }

  ngOnInit() {
    this.router.params.subscribe((params) => {
      const id = params['id'];
      this.title = params['name'];
      this._LibraryServices.getMoviesByGenre(id).subscribe(res => {
        this.movies = res['results'];
      });
    })
  }
}
