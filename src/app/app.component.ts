import { Component, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { LibraryService } from './services/library.service';
import { Genre } from './genre';
import { MovieItemComponent } from './movie-item/movie-item.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'filmoteca';
  genres: Array<Object[]>;
  constructor(injector: Injector,private _libraryServices: LibraryService) {
    const MovieItemElement = createCustomElement(MovieItemComponent, {injector});
    // Register the custom element with the browser.
    customElements.define('movie-item', MovieItemElement);
    this._libraryServices.getGenres().subscribe((data: Array<Genre>) => this.genres = data["genres"].slice(0,20));
  }
}
