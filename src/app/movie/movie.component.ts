import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { LibraryService } from '../services/library.service';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {
  movie: Object;
  reviews: Array<Object>;
  similarMovies: Array<Object>;
  cast: Array<Object>;
  video: Object;
  constructor(
    private _librarServices: LibraryService,
    private router: ActivatedRoute,
    private sanitizer: DomSanitizer
    ) {

  }

  ngOnInit() {
    this.router.params.subscribe((params) => {
      const id = params['id'];
      this._librarServices.getMovie(id).subscribe(movie => {
        this.movie = movie;
      });

      this._librarServices.getMovieCredits(id).subscribe(res => {
        res['cast'] = res['cast'].filter((item) => {return item.profile_path});
        this.cast = res['cast'].slice(0,4);
      });
      this._librarServices.getMovieVideos(id).subscribe(res => {
        if(res['results'] && res['results'].length) {
          this.video = res['results'][0];        
          this.video['url'] = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + this.video['key']);
        }
      });
    })
  }

}