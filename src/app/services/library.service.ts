import { Injectable } from '@angular/core';
import{ HttpClient, HttpParams } from '@angular/common/http';
import { Genre } from '../genre';

@Injectable({
  providedIn: 'root'
})
export class LibraryService {

  apikey: string;
  language: string;
  httpParams = new HttpParams();

  constructor(private _client: HttpClient) {
    this.apikey = '19c58e541d3a5425b2ad7c90cb28666c';
    this.language = 'pt-BR';
    this.httpParams = new HttpParams()
    .set('language', this.language)
    .set('api_key', this.apikey);
    console.log('Api Service Up');
  }
  getGenres() {
    return this._client.get<Array<Genre>>('https://api.themoviedb.org/3/genre/movie/list?'+this.httpParams.toString());
  }
  getMoviesByGenre(id: string) {
    return this._client.get('https://api.themoviedb.org/3/genre/'+ id +'/movies?'+this.httpParams.toString());
  }
  getPopular() {
    this.httpParams = this.httpParams.set('sort_by','popularity.desc');
    return this._client.get('https://api.themoviedb.org/3/discover/movie?'+this.httpParams.toString());
  }
  discoverMovies(searchStr: string) {
    this.httpParams = this.httpParams.set('sort_by','popularity.desc');
    this.httpParams = this.httpParams.set('query', searchStr);
    return this._client.get('https://api.themoviedb.org/3/search/movie?'+this.httpParams.toString());
  }
  getMovie(id: string) {
    return this._client.get('https://api.themoviedb.org/3/movie/'+ id +'?' +  this.httpParams.toString());
  }
  getMovieVideos(id: string) {
    return this._client.get('https://api.themoviedb.org/3/movie/'+ id +'/videos?' +  this.httpParams.toString());
  }
  getMovieCredits(id: string) {
    return this._client.get('https://api.themoviedb.org/3/movie/'+ id +'/credits?' + this.httpParams.toString());
  }
}
