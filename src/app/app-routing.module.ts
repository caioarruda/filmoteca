import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GenresComponent } from './genres/genres.component';
import { MoviesComponent } from './movies/movies.component';
import { MovieComponent } from './movie/movie.component';


const routes: Routes = [
  {path: '', component: MoviesComponent},
  {path: 'genres/:id/:name', component: GenresComponent},
  {path: 'movie/:id', component: MovieComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
