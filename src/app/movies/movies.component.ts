import { Component, OnInit } from '@angular/core';
import { LibraryService } from '../services/library.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {

  popularList: Object;
  searchRes: Object;
  searchStr: string;
  constructor(private _libraryService: LibraryService, private router: ActivatedRoute) {
    this._libraryService.getPopular().subscribe(res => {
      this.popularList = res['results'];
    });
  }

  ngOnInit() {
  }

  searchMovies() {
    this._libraryService.discoverMovies(this.searchStr).subscribe(res => {
      this.searchRes = res['results'];
      console.log(this.searchRes);
    })
  }


}
